package hunt.view;

import java.util.ArrayList;

import hunt.controller.CheckForCollision;
import hunt.controller.InterpretInputs;
import hunt.controller.PlayerSwitch;
import hunt.data.CreatePlayers;
import hunt.data.MapTile;
import hunt.data.Player;
import hunt.data.Position;
import hunt.data.ReadMap;
import hunt.logging.LogPlayerInfo;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.input.GestureEvent;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class HuntMain extends Application {

	private Stage stage;
	public static final int SIZE = 50;
	public static final int PLAYERSPEED = 50;
	ArrayList<Player> players = new ArrayList<>();
	private int activePlayer = 0;
	
	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//Erstellung des Javafx-Fensters
		stage = primaryStage;
		stage.setTitle("Hunt");
		Group root = new Group();
		Canvas canvas = new Canvas(500, 500);
		root.getChildren().add(canvas);
		Scene scene = new Scene(root);
		stage.setScene(scene);
		
		
		//readMapFile liest die Map unter ./resources/Map.txt aus und gibt die Map als auswertbare ArrayList aus
		ArrayList<ArrayList<MapTile>> list = ReadMap.readMapFile();
		
		//createPlayers erstellt eine ArrayList<Player>, die mit Spieler-Objekten bef�llt wird
		//Parameter: Anzahl an Spielern, die erstellt werden sollen
		ArrayList<Player> players = CreatePlayers.createPlayers(2);
		ArrayList<Position> spawnpoints = InterpretInputs.findSpawnPoint(list);
		
		//Methode, die noch vor dem ersten Frame ausgef�hrt wird, um das Spielfeld korrekt zu drawen.
		InterpretInputs.gameStart(canvas, list, players, spawnpoints);
		
		//Initialisiert das fx-Frame mit den gemalten Feldern
		stage.show();
		//KeyInputHandler f�r alle m�glichen Inputs
		 scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
	            @Override
	            public void handle(KeyEvent event) {
	            			Position comparisonOldPos;
	                switch (event.getCode()) {
	                    case UP:   
	                    	comparisonOldPos = players.get(activePlayer).getPos();
	                    	if (CheckForCollision.checkForCollision(list, players, activePlayer, 1)) {
	                    	players.get(activePlayer).setPos(new Position((players.get(activePlayer).getPos().getPosX()), (players.get(activePlayer).getPos().getPosY() - PLAYERSPEED)));
	                    	LogPlayerInfo.logPos(players.get(activePlayer).getPos(), comparisonOldPos);
	                    	InterpretInputs.gameUpdate(canvas, list, players); 
	                    	}
	                    	
	                    	System.out.println("hoch"); 
	                    	break;
	                    case DOWN:
	                    	comparisonOldPos = players.get(activePlayer).getPos();
	                    	if (CheckForCollision.checkForCollision(list, players, activePlayer, 3)) {
	                    	players.get(activePlayer).setPos(new Position((players.get(activePlayer).getPos().getPosX()), (players.get(activePlayer).getPos().getPosY() + PLAYERSPEED)));
	                    	LogPlayerInfo.logPos(players.get(activePlayer).getPos(), comparisonOldPos);
	                    	InterpretInputs.gameUpdate(canvas, list, players);
	                    	}
	                    	
	                    	System.out.println("runter"); 
	                    	break;
	                    case LEFT:
	                    	comparisonOldPos = players.get(activePlayer).getPos();
	                    	if (CheckForCollision.checkForCollision(list, players, activePlayer, 4)) {
	                    	players.get(activePlayer).setPos(new Position((players.get(activePlayer).getPos().getPosX() - PLAYERSPEED), (players.get(activePlayer).getPos().getPosY())));
	                    	LogPlayerInfo.logPos(players.get(activePlayer).getPos(), comparisonOldPos);
	                    	InterpretInputs.gameUpdate(canvas, list, players); 
	                    	}
	                    	
	                    	System.out.println("links"); 
	                    	break;
	                    case RIGHT: 
	                    	comparisonOldPos = players.get(activePlayer).getPos();
	                    	if (CheckForCollision.checkForCollision(list, players, activePlayer, 2)) {
	                    	players.get(activePlayer).setPos(new Position((players.get(activePlayer).getPos().getPosX() + PLAYERSPEED), (players.get(activePlayer).getPos().getPosY())));
	                    	LogPlayerInfo.logPos(players.get(activePlayer).getPos(), comparisonOldPos);
	                    	InterpretInputs.gameUpdate(canvas, list, players);
	                    	}
	                    	
	                    	System.out.println("rechts"); 
	                    	break;
	                    default:
	                    	activePlayer = PlayerSwitch.switchToNextPlayer(activePlayer, players);
	                    	System.out.println("test");
	                    	break;
	                }
	            } 
	        });
	}
}

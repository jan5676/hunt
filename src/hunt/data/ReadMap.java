package hunt.data;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

import hunt.view.HuntMain;
import javafx.scene.paint.Color;

public class ReadMap {
	
	private static final int SIZE = HuntMain.SIZE;
	private static int count;
	private static ArrayList<ArrayList<MapTile>> list;
	
	public static ArrayList<ArrayList<MapTile>> readMapFile() {
		try {
			list = new ArrayList<>();
			Stream<String> mapStream = Files.lines(Paths.get("./resource/Map.txt"));
			mapStream.forEach(e -> list.add(extract(e.toCharArray())));
			mapStream.close();
		} catch (Exception e) {
			System.err.println("Du kannst nicht programmieren");
		}
		
		if (list != null) {
			return list;
		} else {
			System.err.println("Fehler beim Auslesen der Map. Versuche es erneut!");
			System.exit(0);
		}
		
		return null;
	}

	// Umwandlung von Chars zu einem MapTile-Array
	public static ArrayList<MapTile> extract(char[] e) {
		ArrayList<MapTile> list = new ArrayList<>();
		for (int i = 0; i < e.length; i++) {
			String entityType = null;
			Color color = Color.GRAY;
			switch (e[i]) {
			case 'o':
				color = Color.GREENYELLOW;
				entityType = "ground";
				break;
			case 'x':
				color = Color.SADDLEBROWN;
				entityType = "wall";
				break;
			case 's':
				color = Color.YELLOW;
				entityType = "spawnpoint";
				break;
			default:
				break;
			}
			list.add(new MapTile(new Position(i * SIZE, count * SIZE), color, entityType));
		}
		count++;
		return list;
	}
}

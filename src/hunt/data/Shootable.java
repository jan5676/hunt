package hunt.data;

public interface Shootable {

	void shoot();
}

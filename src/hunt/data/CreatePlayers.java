package hunt.data;

import java.util.ArrayList;

import javafx.scene.paint.Color;

public class CreatePlayers {
	
	private static ArrayList<Player> players = new ArrayList<>();
	
	public static ArrayList<Player> createPlayers(int playerCount) {
		for (int i = 0; i < playerCount; i++) {
			players.add(new Player(new Position(100,100), Color.BLACK, 100, null, ""));
		}
		if (players != null) {
			return players;
		} else {
			players.add(new Player(new Position(100,100), Color.BLACK, 100, null, ""));
			return players;
		}		
	}
}

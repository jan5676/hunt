package hunt.data;

public interface Usable {

	void use();
}

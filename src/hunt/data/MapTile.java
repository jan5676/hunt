package hunt.data;

import javafx.scene.paint.Color;

public class MapTile {

	private Position pos;
	protected Color color;
	private String type;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public MapTile(Position pos, Color color, String type) {
		this.pos = pos;
		this.color = color;
		this.type = type;
	}

	public Position getPos() {
		return pos;
	}

	public void setPos(Position pos) {
		this.pos = pos;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	
}

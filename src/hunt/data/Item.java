package hunt.data;

public abstract class Item implements Usable {
	private String name;
	private String desc;
	private int uses;
	
	public Item(String name, String desc, int uses) {
		this.name = name;
		this.desc = desc;
		this.uses = uses;
	}
	
	@Override
	public void use() {
		System.out.println("Not implemented.");
	}
}

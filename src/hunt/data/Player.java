package hunt.data;

import javafx.scene.paint.Color;

public class Player {
	private Inventory inventory;
	private int health;
	private Position pos;
	private Color color;
	private String currentTile;
	
	
	public String getCurrentTile() {
		return currentTile;
	}


	public void setCurrentTile(String currentTile) {
		this.currentTile = currentTile;
	}


	public Color getColor() {
		return color;
	}


	public void setColor(Color color) {
		this.color = color;
	}


	public Position getPos() {
		return pos;
	}


	public Inventory getInventory() {
		return inventory;
	}


	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}


	public int getHealth() {
		return health;
	}


	public void setHealth(int health) {
		this.health = health;
	}


	@Override
	public String toString() {
		return "Player [inventory=" + inventory + ", health=" + health + "]";
	}


	public Player(Position pos, Color color, int health, Inventory inventory, String currentTile) {
		this.pos = pos;
		this.color = color;
		this.health = health;
		this.inventory = inventory;
		this.currentTile = currentTile;
	}

	public void setPos(Position pos) {
		this.pos = pos;
	}
}

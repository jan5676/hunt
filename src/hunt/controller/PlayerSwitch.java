package hunt.controller;

import java.util.ArrayList;

import hunt.data.Player;

public class PlayerSwitch {
	public static int switchToNextPlayer(int activePlayer, ArrayList<Player> players) {
		int playerCount = players.size();
		System.out.println("Anzahl Spieler: " + playerCount);
		if(activePlayer < playerCount - 1) {
			activePlayer++;
		} else {
			activePlayer = 0;
		}
		System.out.println("Aktiver Spieler: " + activePlayer);
		return activePlayer;
	}
}

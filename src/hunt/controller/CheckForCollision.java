package hunt.controller;

import java.util.ArrayList;

import hunt.data.MapTile;
import hunt.data.Player;
import hunt.view.HuntMain;

public class CheckForCollision {
	
	private static int directionC;
	private static int activePlayerC;
	private static final int SIZE = HuntMain.SIZE; 
	
	//direction: 1 = hoch, 2 = rechts, 3 = runter, 4 = links
	//gibt bei Kollision false zur�ck, bei freiem Weg true
	public static boolean checkForCollision(ArrayList<ArrayList<MapTile>> mapTileList, ArrayList<Player> players, int activePlayer, int direction) {
		directionC = direction;
		activePlayerC = activePlayer;
		if(checkForOutOfBounds(mapTileList, players) && checkForPlayerCollision() && checkForWallCollision()) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean checkForOutOfBounds(ArrayList<ArrayList<MapTile>> mapTileList, ArrayList<Player> players) {
		switch(directionC) {
		case 1:
			if ((players.get(activePlayerC).getPos().getPosY() - SIZE) >= 0) {
				return true;
			} else {
				System.out.println("Kann nicht nach oben, da du sonst aus dem Spielfeld gehen w�rdest.");
				return false;
			}
		case 2:
			System.out.println("Position X-Achse: " + mapTileList.get(0).size() * SIZE);
			System.out.println("Position Y_Achse: " + mapTileList.size() * SIZE);
			if ((players.get(activePlayerC).getPos().getPosX() + SIZE) <= (mapTileList.get(0).size() - 1) * SIZE) {
				return true;
			} else {
				System.out.println("Kann nicht nach rechts, da du sonst aus dem Spielfeld gehen w�rdest.");
				return false;
			}
		case 3:
			if ((players.get(activePlayerC).getPos().getPosY() + SIZE) <= (mapTileList.size() - 1) * SIZE) {
				return true;
			} else {
				System.out.println("Kann nicht nach unten, da du sonst aus dem Spielfeld gehen w�rdest.");
				return false;
			}
		case 4:
			if ((players.get(activePlayerC).getPos().getPosX() - SIZE) >= 0) {
				return true;
			} else {
				System.out.println("Kann nicht nach links, da du sonst aus dem Spielfeld gehen w�rdest.");
				return false;
			}
		}
		return true;
	}
	
	public static boolean checkForPlayerCollision() {
		switch(directionC) {
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		}
		return true;
	}
	
	public static boolean checkForWallCollision() {
		switch(directionC) {
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		}
		return true;
	}
}

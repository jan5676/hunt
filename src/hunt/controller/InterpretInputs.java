package hunt.controller;

import java.util.ArrayList;

import hunt.data.MapTile;
import hunt.data.Player;
import hunt.data.Position;
import hunt.logging.LogPlayerInfo;
import hunt.view.HuntMain;
import javafx.scene.canvas.Canvas;

public class InterpretInputs {
	
	private static final int SIZE = HuntMain.SIZE;
	private static String spawnPointType = null;
	private static Position spawnPointPosition = null;
	
	public static ArrayList<Position> findSpawnPoint(ArrayList<ArrayList<MapTile>> list) {
		ArrayList<Position> spawnpoints = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.get(i).size(); j++) {
				spawnPointType = list.get(i).get(j).getType();
				spawnPointPosition = list.get(i).get(j).getPos();
				if (spawnPointType.equals("spawnpoint")) {
					spawnpoints.add(spawnPointPosition);
				}
			}
		}
		spawnpoints.forEach(e -> System.out.println(e.toString()));
		return spawnpoints;
	}
	
	public static void gameStart(Canvas canvas, ArrayList<ArrayList<MapTile>> list, ArrayList<Player> player, ArrayList<Position> spawnpoints) {
		list.forEach(e -> e.forEach(f -> drawTile(canvas, f)));
		player.forEach(e -> drawPlayerAtSpawnpoint(canvas, e, list, spawnpoints));
		LogPlayerInfo.logMapTile(player);
	}
	
	public static void gameUpdate(Canvas canvas, ArrayList<ArrayList<MapTile>> list, ArrayList<Player> player) {
		list.forEach(e -> e.forEach(f -> drawTile(canvas, f)));
		player.forEach(e -> drawPlayer(canvas, e, list));
		LogPlayerInfo.logMapTile(player);
	}
	
	public static void drawTile(Canvas canvas, MapTile tile) {
		Position p = tile.getPos();
		canvas.getGraphicsContext2D().setFill(tile.getColor());
		canvas.getGraphicsContext2D().fillRect(p.getPosX(), p.getPosY(), SIZE, SIZE);
	}
	
	public static void drawPlayer(Canvas canvas, Player player, ArrayList<ArrayList<MapTile>> list) {
		Position p = player.getPos();
		canvas.getGraphicsContext2D().setFill(player.getColor());
		canvas.getGraphicsContext2D().fillRect(p.getPosX(), p.getPosY(), SIZE, SIZE);
		list.forEach(e -> e.forEach(f -> checkForEntityType(f, player)));
	}
	
	public static void drawPlayerAtSpawnpoint(Canvas canvas, Player player, ArrayList<ArrayList<MapTile>> list, ArrayList<Position> spawnpoints) {
		int randomSpawnPoint = (int) (Math.random() * spawnpoints.size());
		player.setPos(spawnpoints.get(randomSpawnPoint));
		canvas.getGraphicsContext2D().setFill(player.getColor());
		canvas.getGraphicsContext2D().fillRect(player.getPos().getPosX(), player.getPos().getPosY(), SIZE, SIZE);
		list.forEach(e -> e.forEach(f -> checkForEntityType(f, player)));
	}
	
	public static void checkForEntityType(MapTile mapTile, Player player) {
		int playerPosX = player.getPos().getPosX();
		int playerPosY = player.getPos().getPosY();
		int tilePosX = mapTile.getPos().getPosX();
		int tilePosY = mapTile.getPos().getPosY();
		if (playerPosX == tilePosX && playerPosY == tilePosY) {
			player.setCurrentTile(mapTile.getType());
		} else {
			//System.err.println("Fehler beim vergleichen der Positionen.");
		}
		
	}
	
	
//	public static void handleKeyInput(Player player1, Scene scene, Canvas canvas) {
//		scene.setOnKeyPressed(e -> handle(e));
//	}
//	public static void handle(KeyEvent event) {
//    	//player1.setPos(new Position(player1.getPos().getPosX() - 50, player1.getPos().getPosY()));
//    	
//        switch (event.getCode()) {
//            case UP:    
//            	playerPosition.setPosY(playerPosition.getPosY() - 50);
//            	//player1.setPos(playerPosition);
//            	gameUpdate(canvas, list, player); 
//            	System.out.println("hoch"); 
//            	break;
//            case DOWN:  
//            	playerPosition.setPosY(playerPosition.getPosY() + 50);
//            	//player1.setPos(playerPosition);
//            	gameUpdate(canvas, list, player); 
//            	System.out.println("runter"); 
//            	break;
//            case LEFT:  
//            	playerPosition.setPosX(playerPosition.getPosX() - 50);
//            	//player1.setPos(playerPosition);
//            	gameUpdate(canvas, list, player); 
//            	System.out.println("links"); 
//            	break;
//            case RIGHT: 
//            	playerPosition.setPosX(playerPosition.getPosX() + 50);
//            	//player1.setPos(playerPosition);
//            	gameUpdate(canvas, list, player); 
//            	System.out.println("rechts"); 
//            	break;
//        }
//	}
	
	}

